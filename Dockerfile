FROM debian:bullseye-slim
RUN DEBIAN_FRONTEND=noninteractive apt-get update; apt-get install -y php-fpm \
    nginx && apt-get clean && rm -rf /var/lib/apt/lists/*
EXPOSE 80
ADD --chown=root:root docker-entrypoint.sh /
RUN mkdir /run/php/
RUN echo 'catch_workers_output = yes\nclear_env = no' >> /etc/php/7.4/fpm/pool.d/www.conf; \
sed -i 's/error_log = .*/error_log = \/dev\/stdout/' /etc/php/7.4/fpm/php-fpm.conf
ENTRYPOINT /docker-entrypoint.sh
