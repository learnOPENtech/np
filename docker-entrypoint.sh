#!/bin/bash

php-fpm7.4 --fpm-config /etc/php/7.4/fpm/php-fpm.conf --nodaemonize &
nginx -g 'daemon off; error_log stderr info;' &
wait -n
exit $?
